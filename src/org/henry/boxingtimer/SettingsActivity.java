package org.henry.boxingtimer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;

public class SettingsActivity extends Activity {
	
	public static final String ROUND_PICKER = "SELECTED_ROUND";
	public static final String MINUTE_PICKER = "SELECTED_MINUTES";
	public static final String REST_PICKER = "SELECTED_REST_TIME";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);

		NumberPicker roundPicker = (NumberPicker) findViewById(R.id.roundPicker);
		roundPicker.setMinValue(1);
		roundPicker.setMaxValue(20);

		NumberPicker minutePicker = (NumberPicker) findViewById(R.id.minutePicker);
		minutePicker.setMinValue(1);
		minutePicker.setMaxValue(5);

		NumberPicker restPicker = (NumberPicker) findViewById(R.id.restPicker);
		restPicker.setMinValue(30);
		restPicker.setMaxValue(60);

		Button doneBtn = configDoneBtn(roundPicker, minutePicker, restPicker);

	}

	private Button configDoneBtn(final NumberPicker roundPicker,
			final NumberPicker minutePicker, final NumberPicker restPicker) {
		Button doneBtn = (Button) findViewById(R.id.button_done);
		final Context ctx = this;

		doneBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// MainActivity.setRoundMinute( minutePicker.getValue());
				// MainActivity.setRounds(roundPicker.getValue());
				// MainActivity.setRestPicker(restPicker.getValue());

				Intent mainActivityIntent = new Intent(ctx, MainActivity.class);
				mainActivityIntent.putExtra(ROUND_PICKER,
						roundPicker.getValue());
				mainActivityIntent.putExtra(MINUTE_PICKER,
						minutePicker.getValue());
				mainActivityIntent.putExtra(REST_PICKER, restPicker.getValue());
				startActivity(mainActivityIntent);
			}
		});

		doneBtn.setTextSize(getResources().getDimension(R.dimen.btnTextsize));
		return doneBtn;
	}

	
}
