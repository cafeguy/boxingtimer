package org.henry.boxingtimer;

import org.henry.util.BoxingTimerUtil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CopyOfMainActivity extends Activity {

	private static int totalRounds = 3;
	private static long timeMilliSec = 5000;
	private static long restMs = 10000;
	private static Button stopBtn;
	private static Button startBtn;
	private static Button settingsBtn;
	private TextView roundView;
	private TextView timerView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		roundView = (TextView) findViewById(R.id.text_round);
		String text = roundView.getText() + " " + totalRounds;
		roundView.setTextSize(getResources().getDimension(R.dimen.textsize));
		roundView.setText(text);

		timerView = (TextView) findViewById(R.id.text_timer);
		timerView.setTextSize(getResources().getDimension(R.dimen.textsize));
		// timerView.setTextSize(R.dimen.textsize);

		CountDownTimer timer = startTimer(timerView, roundView);

		startBtn = configStartBtn(timer);

		stopBtn = configStopBtn(timer);
		stopBtn.setVisibility(View.INVISIBLE);

		settingsBtn = configSettingsBtn();

	}

	private Button configSettingsBtn() {
		Button button = (Button) findViewById(R.id.button_settings);
		final Context ctx = this;

		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent settingsIntent = new Intent(ctx, SettingsActivity.class);
				startActivity(settingsIntent);
			}
		});

		button.setTextSize(getResources().getDimension(R.dimen.btnTextsize));
		return button;
	}

	private Button configStartBtn(final CountDownTimer timer) {
		Button button = (Button) findViewById(R.id.button_start);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// startTimer(timerView, roundView);
				timer.start();
				stopBtn.setVisibility(View.VISIBLE);
				startBtn.setVisibility(View.INVISIBLE);
				settingsBtn.setVisibility(View.INVISIBLE);
			}
		});

		button.setTextSize(getResources().getDimension(R.dimen.btnTextsize));
		return button;
	}

	private Button configStopBtn(final CountDownTimer timer) {
		Button button = (Button) findViewById(R.id.button_stop);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				timer.cancel();
				stopBtn.setVisibility(View.INVISIBLE);
				startBtn.setVisibility(View.VISIBLE);
				settingsBtn.setVisibility(View.VISIBLE);
				timerView.setText("Timer: " + formatSecs(timeMilliSec));
			}
		});

		button.setTextSize(getResources().getDimension(R.dimen.btnTextsize));
		return button;
	}

	private CountDownTimer startTimer(final TextView timerView,
			final TextView roundView) {
		if (totalRounds == 0) {
			return null;
		}

		CountDownTimer timer = new CountDownTimer(timeMilliSec, 1000) {

			public void onTick(long millisUntilFinished) {
				timerView.setText("Timer : " + (millisUntilFinished / 60000)
						+ ":" + formatSecs(millisUntilFinished));
			}

			public void onFinish() {

				if (totalRounds > 0) {
					totalRounds--;

					if (totalRounds > 0) {
						BoxingTimerUtil.startRestTimer(this, timerView,
								roundView, restMs);
					} else {
						startBtn.setVisibility(View.VISIBLE);
						stopBtn.setVisibility(View.INVISIBLE);
						settingsBtn.setVisibility(View.VISIBLE);
						roundView.setText("GOOD JOB!!!");
					}

				}
				roundView.setText("Round " + String.valueOf(totalRounds));
				timerView.setText("Timer : 0:00");
				playSound(RingtoneManager.TYPE_ALARM);

			}

		};

		return timer;
	}

	private String formatSecs(long milliSecs) {
		long secs = (milliSecs % 60000) / 1000;

		if (secs < 10) {
			return "0" + secs;
		} else {
			return String.valueOf(secs);
		}
	}

	private void playSound(int sound) {
		Uri notification = RingtoneManager.getDefaultUri(sound);
		Ringtone ring = RingtoneManager.getRingtone(getApplicationContext(),
				notification);
		ring.play();
	}

	public static void setRoundMinute(int minutesPerRnd) {
		timeMilliSec = minutesPerRnd * 60000;
	}

	public static void setRounds(int numOfRnds) {
		totalRounds = numOfRnds;
	}

	public static void setRestPicker(int restSecs) {
		restMs = restSecs * 1000;
	}

}
