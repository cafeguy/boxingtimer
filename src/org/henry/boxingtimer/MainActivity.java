package org.henry.boxingtimer;

import org.henry.util.BoxingTimerUtil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

	private int totalRounds = 3;
	private long timeMilliSec = 5000;
	private long restMs = 20000;
	private Button stopBtn;
	private Button startBtn;
	private Button settingsBtn;
	private TextView roundView;
	private TextView timerView;
	private CountDownTimer roundTimer;
	private int currentRound = 1;
	private CountDownTimer restTimer;

	private void getIntentData() {
		if (getIntent() != null && getIntent().getExtras() != null) {
			Bundle bundle = getIntent().getExtras();
			totalRounds = bundle.getInt(SettingsActivity.ROUND_PICKER);
			timeMilliSec = bundle.getInt(SettingsActivity.MINUTE_PICKER) * 60000;
			restMs = bundle.getInt(SettingsActivity.REST_PICKER) * 1000;
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		getIntentData();

		roundView = (TextView) findViewById(R.id.text_round);
		roundView.setTextSize(getResources().getDimension(R.dimen.textsize));
		displayCurrentRound();

		timerView = (TextView) findViewById(R.id.text_timer);
		timerView.setTextSize(getResources().getDimension(R.dimen.textsize));

		CountDownTimer timer = startTimer(timerView, roundView);

		startBtn = configStartBtn(timer);

		stopBtn = configStopBtn(timer);
		stopBtn.setVisibility(View.INVISIBLE);

		settingsBtn = configSettingsBtn();

	}

	private Button configSettingsBtn() {
		Button button = (Button) findViewById(R.id.button_settings);
		final Context ctx = this;

		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent settingsIntent = new Intent(ctx, SettingsActivity.class);
				startActivity(settingsIntent);
			}
		});

		button.setTextSize(getResources().getDimension(R.dimen.btnTextsize));
		return button;
	}

	private Button configStartBtn(final CountDownTimer timer) {
		Button button = (Button) findViewById(R.id.button_start);

		button.setTextSize(getResources().getDimension(R.dimen.btnTextsize));
		return button;
	}

	public void startRound(View view) {
		if (currentRound == totalRounds) {
			currentRound = 1;
		}
		// displayCurrentRound();
		roundTimer = startTimer(timerView, roundView);
		roundTimer.start();
		stopBtn.setVisibility(View.VISIBLE);
		startBtn.setVisibility(View.INVISIBLE);
		settingsBtn.setVisibility(View.INVISIBLE);
	}

	public void stopRound(View view) {
		roundTimer.cancel();
		if (restTimer != null) {
			restTimer.cancel();
		}
		stopBtn.setVisibility(View.INVISIBLE);
		startBtn.setVisibility(View.VISIBLE);
		settingsBtn.setVisibility(View.VISIBLE);
		timerView.setTextColor(Color.RED);
		timerView.setText("Timer: " + (timeMilliSec / 60000) + ":"
				+ formatSecs(timeMilliSec));
	}

	private Button configStopBtn(final CountDownTimer timer) {
		Button button = (Button) findViewById(R.id.button_stop);
		button.setTextSize(getResources().getDimension(R.dimen.btnTextsize));
		return button;
	}

	private CountDownTimer startTimer(final TextView timerView,
			final TextView roundView) {
		if (totalRounds == 0) {
			return null;
		}

		CountDownTimer timer = new CountDownTimer(timeMilliSec, 1000) {

			public void onTick(long millisUntilFinished) {
				displayCurrentRound();
				timerView.setTextColor(Color.GREEN);
				timerView.setText("Timer: " + (millisUntilFinished / 60000)
						+ ":" + formatSecs(millisUntilFinished));
			}

			public void onFinish() {

				if (currentRound < totalRounds) {
					restTimer = startRestTimer(this, timerView, roundView,
							restMs);
					// BoxingTimerUtil.startRestTimer(this, timerView,
					// roundView, restMs);
					currentRound++;
				} else {
					startBtn.setVisibility(View.VISIBLE);
					stopBtn.setVisibility(View.INVISIBLE);
					settingsBtn.setVisibility(View.VISIBLE);
					roundView.setText("GOOD JOB!!!");
				}

				timerView.setText("Timer: 0:00");
				playSound(RingtoneManager.TYPE_ALARM);

			}

		};

		return timer;
	}

	private String formatSecs(long milliSecs) {
		long secs = (milliSecs % 60000) / 1000;

		if (secs < 10) {
			return "0" + secs;
		} else {
			return String.valueOf(secs);
		}
	}

	private void playSound(int sound) {
		Uri notification = RingtoneManager.getDefaultUri(sound);
		Ringtone ring = RingtoneManager.getRingtone(getApplicationContext(),
				notification);
		ring.play();
	}

	public void setRoundMinute(int minutesPerRnd) {
		timeMilliSec = minutesPerRnd * 60000;
	}

	public void setRounds(int numOfRnds) {
		totalRounds = numOfRnds;
	}

	public void setRestPicker(int restSecs) {
		restMs = restSecs * 1000;
	}

	public void displayCurrentRound() {
		// playSound(RingtoneManager.TYPE_NOTIFICATION);
		roundView.setText("Round: " + currentRound + "/" + totalRounds);
	}

	private static long warmUpTime = 10000;

	public CountDownTimer startRestTimer(final CountDownTimer roundTimer,
			final TextView timerText, final TextView rndText,
			final long restMilliSecs) {

		CountDownTimer countDown = new CountDownTimer(restMilliSecs, 1000) {

			@Override
			public void onTick(long millisUntilFinished) {
				timerText.setText("Rest: " + (millisUntilFinished / 60000)
						+ ":" + formatSecs(millisUntilFinished));

				if ("10".equalsIgnoreCase(formatSecs(millisUntilFinished))) {
					soundWarmUpNotification();
				}
			}

			@Override
			public void onFinish() {
				roundTimer.start();
			}
		};

		countDown.start();
		return countDown;
	}

	private void soundWarmUpNotification() {
		playSound(RingtoneManager.TYPE_NOTIFICATION);
	}

}
