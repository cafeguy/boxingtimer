package org.henry.util;

import android.os.CountDownTimer;
import android.widget.TextView;

public class BoxingTimerUtil {

	public static CountDownTimer startRestTimer(final CountDownTimer roundTimer,
			final TextView timerText, final TextView rndText, final long restMilliSecs) {
		CountDownTimer countDown = new CountDownTimer(restMilliSecs, 1000) {

			@Override
			public void onTick(long millisUntilFinished) {
				timerText.setText("Rest: " + (millisUntilFinished / 60000)
						+ ":" + formatSecs(millisUntilFinished));
			}

			@Override
			public void onFinish() {
				roundTimer.start();
			}
		};

		countDown.start();
		return countDown;
	}
	
	private static String formatSecs(long milliSecs) {
		long secs = (milliSecs % 60000) / 1000;

		if (secs < 10) {
			return "0" + secs;
		} else {
			return String.valueOf(secs);
		}
	}
}
